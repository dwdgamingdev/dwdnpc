package net.downwithdestruction.dwdnpc.packet;

import java.lang.reflect.Field;
import net.downwithdestruction.dwdnpc.DwDNPC;
import net.downwithdestruction.dwdnpc.npclib.mob.Mob;
import net.downwithdestruction.dwdnpc.npclib.mob.MobType;
import net.minecraft.server.v1_5_R2.MathHelper;
import net.minecraft.server.v1_5_R2.Packet24MobSpawn;
import net.minecraft.server.v1_5_R2.Packet29DestroyEntity;
import net.minecraft.server.v1_5_R2.Packet32EntityLook;
import net.minecraft.server.v1_5_R2.Packet35EntityHeadRotation;
import net.minecraft.server.v1_5_R2.Packet40EntityMetadata;
import org.bukkit.ChatColor;
import org.bukkit.Location;
//import net.minecraft.server.v1_4_6.Packet5EntityEquipment;

public class PacketGenerator {
	private final Mob mob;
	
	protected int encposX;
	protected int encposY;
	protected int encposZ;
	protected boolean firstpos = true;
	
	public PacketGenerator(final Mob mob) {
		this.mob = mob;
	}
	
	public byte degreeToByte(float degree) {
		return (byte) ((int) degree * 256.0F / 360.0F);
	}
	
	public int[] getLocationVariables(Location loc) {
		int x = MathHelper.floor(loc.getX() *32D);
		int y = MathHelper.floor(loc.getY() *32D);
		int z = MathHelper.floor(loc.getZ() *32D);
		if(firstpos) {
			encposX = x;
			encposY = y;
			encposZ = z;
			firstpos = false;
		}
		return new int[] {x, y, z};
	}
	
	/*public LinkedList<Packet> getArmorPackets(Player player) {
		LinkedList<Packet> packets = new LinkedList<Packet>();
		ItemStack[] armor = player.getInventory().getArmorContents();
		for (byte i=0; i < armor.length; i++) {
			packets.add(getEquipmentChangePacket((short) (i + 1), armor[i]));
		}
		return packets;
	}*/
	
	public Packet24MobSpawn getMobSpawnPacket(Location loc) {
		int[] locVars = getLocationVariables(loc);
		Packet24MobSpawn packet = new Packet24MobSpawn();
		packet.a = mob.entityID;
		packet.b = mob.type.id;
		packet.c = locVars[0];
		packet.d = locVars[1];
		packet.e = locVars[2];
		packet.i = degreeToByte(loc.getYaw());
		packet.j = degreeToByte(loc.getPitch());
		if (mob.type == MobType.EnderDragon) { // Ender Dragon fix
			packet.i = (byte) (packet.i - 128);
		} else if (mob.type == MobType.Chicken) { // Chicken fix
			packet.j = (byte) (packet.j * -1);
		}
		packet.k = packet.i;
		try {
			Field metadataField = packet.getClass().getDeclaredField("s");
			metadataField.setAccessible(true);
			metadataField.set(packet, mob.metadata);
		} catch (Exception e) {
			DwDNPC.getPlugin().log(ChatColor.RED + "[SEVERE] Unable to set the metadata for a " + mob.type.name() +  " disguise!");
		}
		return packet;
	}
	
	public Packet29DestroyEntity getEntityDestroyPacket() {
		return new Packet29DestroyEntity(mob.entityID);
	}
	
	/*public Packet5EntityEquipment getEquipmentChangePacket(short slot, ItemStack item) {
		Packet5EntityEquipment packet;
		if (item == null) {
			packet = new Packet5EntityEquipment();
			packet.a = mob.entityID;
			packet.b = slot;
			try{
				Field itemField = packet.getClass().getDeclaredField("c");
				itemField.setAccessible(true);
				itemField.set(packet, null);
			} catch (Exception e) {
				DwDNPC.getPlugin().log(ChatColor.RED + "[SEVERE] Unable to set the item type for a player disguise!");
			}
		} else {
			packet = new Packet5EntityEquipment(mob.entityID, slot, ((CraftItemStack) item).getHandle());
		}
		return packet;
	}*/
	
	public Packet32EntityLook getEntityLookPacket(Location loc) {
		Packet32EntityLook packet = new Packet32EntityLook();
		packet.a = mob.entityID;
		packet.b = 0;
		packet.c = 0;
		packet.d = 0;
		packet.e = degreeToByte(loc.getYaw());
		packet.f = degreeToByte(loc.getPitch());
		if (mob.type == MobType.EnderDragon) { // EnderDragon specific
			packet.e = (byte) (packet.e - 128);
		} else if (mob.type == MobType.Chicken) { // Chicken fix
			packet.f = (byte) (packet.f * -1);
		}
		return packet;
	}
	
	public Packet40EntityMetadata getEntityMetadataPacket() {
		return new Packet40EntityMetadata(mob.entityID, mob.metadata, true);
	}
	
	public Packet35EntityHeadRotation getHeadRotatePacket(Location loc) {
		return new Packet35EntityHeadRotation(mob.entityID, degreeToByte(loc.getYaw()));
	}
}
