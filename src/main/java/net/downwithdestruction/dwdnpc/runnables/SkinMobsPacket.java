package net.downwithdestruction.dwdnpc.runnables;

import java.util.TreeMap;
import net.downwithdestruction.dwdnpc.DwDNPC;
import net.downwithdestruction.dwdnpc.npclib.entity.HumanNPC;
import net.downwithdestruction.dwdnpc.npclib.entity.NPC;
import net.downwithdestruction.dwdnpc.npclib.mob.Mob;
import net.minecraft.server.v1_5_R2.Packet;
import org.bukkit.craftbukkit.v1_5_R2.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class SkinMobsPacket implements Runnable {
	private DwDNPC plugin;
	private final TreeMap<String,String> inRange = new TreeMap<String,String>();
	
	public SkinMobsPacket(DwDNPC plugin) {
		this.plugin = plugin;
	}
	
	@SuppressWarnings("unused")
	@Override
	public void run() {
		for (NPC npc : plugin.npcManager.getNPCs()) {
			if (npc == null) continue;
			final HumanNPC hnpc = (HumanNPC) npc;
			if (hnpc == null) continue;
			final Mob mob = plugin.npcManager.mobDB.get(hnpc.getName());
			if (mob == null) continue;
			final Packet packet = mob.getMobSpawnPacket(hnpc.getPlayer().getLocation());
			if (packet == null) continue;
			for (Player observer : hnpc.getPlayer().getWorld().getPlayers()) {
				if (plugin.withinRange(hnpc.getPlayer().getLocation(), observer.getLocation(), plugin.skinMobsRadius)) {
					if (inRange.containsKey(hnpc.getName() + ";" + observer.getName()))
						continue;
					inRange.put(hnpc.getName() + ";" + observer.getName(), "true");
					if (observer != hnpc.getPlayer()) {
						observer.hidePlayer(hnpc.getPlayer());
						((CraftPlayer) observer).getHandle().playerConnection.sendPacket(packet);
					}
				} else {
					if (inRange.containsKey(hnpc.getName() + ";" + observer.getName()))
						inRange.remove(hnpc.getName() + ";" + observer.getName());
				}
			}
		}
	}
}
