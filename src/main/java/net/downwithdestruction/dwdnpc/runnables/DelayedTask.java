package net.downwithdestruction.dwdnpc.runnables;

import java.util.TreeMap;

import net.downwithdestruction.dwdnpc.DwDNPC;
import net.downwithdestruction.dwdnpc.npclib.entity.HumanNPC;
import net.downwithdestruction.dwdnpc.npclib.entity.NPC;

import org.bukkit.Location;
import org.bukkit.entity.Player;

public class DelayedTask implements Runnable {
	private DwDNPC plugin;
	private final TreeMap<Double,Player> lookat = new TreeMap<Double,Player>();
	private final TreeMap<String,String> msgRange = new TreeMap<String,String>();

	public DelayedTask(DwDNPC plugin) {
		this.plugin = plugin;
	}
	
	@Override
	public void run() {
		for (NPC npc : plugin.npcManager.getNPCs()) {
			final HumanNPC hnpc = (HumanNPC) npc;
			final Location npcLoc = npc.getBukkitEntity().getLocation();
			lookat.clear();
			for (Player player : plugin.getServer().getOnlinePlayers()) {
				if (plugin.isVanished(player))
					continue;
				if (plugin.withinRange(npcLoc, player.getLocation(), hnpc.getLookAtRadius(plugin.lookAtRadius))) {
					lookat.put(npcLoc.distanceSquared(player.getEyeLocation()), player);
				}
				if (hnpc.getMessage() != null && !hnpc.getMessage().equals("")) {
					if (plugin.withinRange(npcLoc, player.getLocation(), hnpc.getMessageRadius())) {
						if (msgRange.containsKey(hnpc.getBukkitEntity().getEntityId() + ";" + player.getName()))
							continue;
						msgRange.put(hnpc.getBukkitEntity().getEntityId() + ";" + player.getName(), "true");
						final String message = hnpc.getMessage()
							.replaceAll("(?i)\\{npc\\}", hnpc.getName())
							.replaceAll("(?i)\\{name\\}", player.getName())
							.replaceAll("(?i)\\{dispname\\}", (player.getDisplayName() != null) ? player.getDisplayName() : player.getName())
							.replaceAll("(?i)\\{world\\}", hnpc.getPlayer().getWorld().getName())
							.replaceAll("(?i)\u00a7[a-f0-9k-or]", "")
							.replaceAll("(?i)&([a-f0-9k-or])", "\u00a7$1");
						player.sendMessage(message);
						plugin.getServer().getConsoleSender().sendMessage(message);
					} else {
						if (msgRange.containsKey(hnpc.getBukkitEntity().getEntityId() + ";" + player.getName()))
							msgRange.remove(hnpc.getBukkitEntity().getEntityId() + ";" + player.getName());
					}
				}
			}
			if (lookat.size() > 0) {
				hnpc.lookAtPoint(lookat.get(lookat.firstKey()).getEyeLocation());
			}
		}
	}
}
