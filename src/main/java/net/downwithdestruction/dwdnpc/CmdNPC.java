package net.downwithdestruction.dwdnpc;

import net.downwithdestruction.dwdnpc.npclib.entity.HumanNPC;
import net.downwithdestruction.dwdnpc.npclib.entity.NPC;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.command.RemoteConsoleCommandSender;
import org.bukkit.entity.Player;

public class CmdNPC implements CommandExecutor {
	private DwDNPC plugin;
	
	public CmdNPC(DwDNPC plugin) {
		this.plugin = plugin;
	}
	
	public boolean isAuthorized(final CommandSender player, final String node) {
		return  player instanceof RemoteConsoleCommandSender ||
				player instanceof ConsoleCommandSender ||
				player.hasPermission("dwdnpc.op") ||
				player.hasPermission(node);
	}
	
	public boolean dispNoPerms(CommandSender sender) {
		sender.sendMessage(ChatColor.RED + "You are not permitted to use this command.");
		return true;
	}
	
	public boolean dispPlayerOnly(CommandSender sender) {
		sender.sendMessage(ChatColor.RED + "This command is only available to players!");
		return true;
	}
	
	public boolean dispForceDelete(CommandSender sender) {
		sender.sendMessage(ChatColor.RED + "Multiple NPC's found with that name!");
		sender.sendMessage(ChatColor.RED + "Please use the 'deleteid' function to specify which NPC to delete.");
		sender.sendMessage(ChatColor.RED + "Or use the '-force' flag to force deletion of ALL NPCs with this name.");
		return true;
	}
	
	private boolean showusage(CommandSender sender) {
		sender.sendMessage(ChatColor.RED + "Function not found! Valid functions: " + 
				ChatColor.GRAY + "create" + ChatColor.RED + ", " + 
				ChatColor.GRAY + "delete" + ChatColor.RED + ", " +
				ChatColor.GRAY + "deleteid" + ChatColor.RED + ", " +
				ChatColor.GRAY + "radius" + ChatColor.RED + ", " +
				ChatColor.GRAY + "reload" + ChatColor.RED + "."
		);
		return false;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(cmd.getName().equalsIgnoreCase("dwdnpc")) {
			if (args.length < 1) return showusage(sender);
			if (args[0].equalsIgnoreCase("create")) {
				if (!isAuthorized(sender, "dwdnpc.create")) return dispNoPerms(sender);
				if (!(sender instanceof Player)) dispPlayerOnly(sender);
				if (args.length < 2) {
					sender.sendMessage(ChatColor.RED + "You did not specify a name for this NPC!");
					sender.sendMessage(ChatColor.GRAY + "/dwdnpc create [name]");
					return true;
				}
				final Location loc = ((Player) sender).getLocation();
				final String name = (args[1].length() > 16) ? args[1].substring(0, 16) : args[1];
				final Integer id = plugin.npcManager.getID(plugin.npcManager.spawnHumanNPC(name, loc));
				if (id == null) {
					sender.sendMessage(ChatColor.RED + "Failed to create NPC");
					return true;
				}
				plugin.getConfig().set("npcs." + id + ".name", name);
				plugin.getConfig().set("npcs." + id + ".world", loc.getWorld().getName());
				plugin.getConfig().set("npcs." + id + ".x", loc.getX());
				plugin.getConfig().set("npcs." + id + ".y", loc.getY());
				plugin.getConfig().set("npcs." + id + ".z", loc.getZ());
				plugin.saveConfig();
				// Reload NPCs (quick fix to remove console spam)
				plugin.reloadConfig();
				plugin.loadConfig();
				plugin.npcManager.despawnAll();
				plugin.npcManager.loadNPCs();
				plugin.stopDelayedTask();
				plugin.startDelayedTask();
				sender.sendMessage(ChatColor.GREEN + "NPC has been created!");
				if(plugin.debug) plugin.log(ChatColor.GREEN + "Created NPC with ID: " + id);
				return true;
			} else if (args[0].equalsIgnoreCase("delete")) {
				if (!isAuthorized(sender, "dwdnpc.delete")) return dispNoPerms(sender);
				if (args.length < 2) {
					sender.sendMessage(ChatColor.RED + "You did not specify a name of an NPC to delete!");
					sender.sendMessage(ChatColor.GRAY + "/dwdnpc delete [name] (-force)");
					return true;
				}
				final String name = (args[1].length() > 16) ? args[1].substring(0, 16) : args[1];
				if (plugin.npcManager.getNPCCountByName(name) < 1) {
					sender.sendMessage(ChatColor.RED + "No NPC found by that name!");
					return true;
				}
				final Integer count = plugin.npcManager.getNPCCountByName(name);
				if (count > 1) {
					if (args.length > 2) {
						if (!args[2].equalsIgnoreCase("-force")) return dispForceDelete(sender);
					} else  return dispForceDelete(sender);
				}
				for (NPC npc : plugin.npcManager.getNPCs()) {
					if (((HumanNPC) npc).getName().equalsIgnoreCase(name)) {
						int id = plugin.npcManager.getID(npc);
						plugin.getConfig().set("npcs." + id, null);
						plugin.saveConfig();
						plugin.npcManager.despawnHuman(id);
						if(plugin.debug) plugin.log(ChatColor.GREEN + "Deleted NPC with ID: " + id);
					}
				}
				sender.sendMessage(ChatColor.GREEN + "NPC" + ((count > 1) ? "s" : "") + " has been deleted.");
				return true;
			} else if (args[0].equalsIgnoreCase("deleteid")) {
				if (!isAuthorized(sender, "dwdnpc.delete")) return dispNoPerms(sender);
				if (args.length < 2) {
					sender.sendMessage(ChatColor.RED + "You did not specify an ID of an NPC to delete!");
					sender.sendMessage(ChatColor.GRAY + "/dwdnpc deleteid [id]");
					return true;
				}
				Integer id;
				try {
					id = Integer.valueOf(args[1]);
				} catch (NumberFormatException npe) {
					sender.sendMessage(ChatColor.RED + "ID must be a number!");
					return true;
				}
				final NPC npc = plugin.npcManager.getNPCByID(id);
				if (npc == null) {
					sender.sendMessage(ChatColor.RED + "NPC not found by that ID!");
					return true;
				}
				plugin.getConfig().set("npcs." + id, null);
				plugin.saveConfig();
				plugin.npcManager.despawnHuman(id);
				if(plugin.debug) plugin.log(ChatColor.GREEN + "Deleted NPC with ID: " + id);
				sender.sendMessage(ChatColor.GREEN + "NPC has been deleted.");
				return true;
			} else if (args[0].equalsIgnoreCase("radius")) {
				if (!isAuthorized(sender, "dwdnpc.admin")) return dispNoPerms(sender);
				if (args.length < 2) {
					sender.sendMessage(ChatColor.RED + "You did not specify an aware radius!");
					sender.sendMessage(ChatColor.GRAY + "/dwdnpc radius [radius]");
					return true;
				}
				Double radius;
				try {
					radius = Double.valueOf(args[1]);
				} catch(NumberFormatException e) {
					sender.sendMessage(ChatColor.RED + "Please enter a valid number!");
					return true;
				}
				if (radius < 1) {
					sender.sendMessage(ChatColor.RED + "Radius must be at least 1!");
					return true;
				} else if (radius > 50) {
					sender.sendMessage(ChatColor.RED + "To prevent lag, values of higher than 50 are not allowed!");
					return true;
				}
				if (args.length > 2) {
					if (args[2].equalsIgnoreCase("-id")) {
						if (args.length < 3) {
							sender.sendMessage(ChatColor.RED + "Missing ID after the -id flag!");
							return true;
						}
						Integer id;
						try {
							id = Integer.valueOf(args[3]);
						} catch (NumberFormatException nfe) {
							sender.sendMessage(ChatColor.RED + "The id given is not a valid number!");
							return true;
						}
						final NPC npc = plugin.npcManager.getNPCByID(id);
						if (npc == null) {
							sender.sendMessage(ChatColor.RED + "NPC not found by that ID!");
							return true;
						}
						plugin.getConfig().set("npcs." + id + ".radius", radius);
						plugin.saveConfig();
						sender.sendMessage(ChatColor.GREEN + "Radius saved. NPC with ID " + id + " now has aware radius of " + ChatColor.GRAY + radius);
						if(plugin.debug) plugin.log(ChatColor.GREEN + "Aware radius for NPC " + id + " is now set to: " + radius);
						return true;
					} else if (args[2].equalsIgnoreCase("-name")) {
						if (args.length < 3) {
							sender.sendMessage(ChatColor.RED + "Missing name after the -name flag!");
							return true;
						}
						final String name = args[3];
						final NPC npc = plugin.npcManager.getNPCByName(name);
						if (npc == null) {
							sender.sendMessage(ChatColor.RED + "NPC not found by that name!");
							return true;
						}
						if (plugin.npcManager.getNPCCountByName(name) > 1) {
							sender.sendMessage(ChatColor.RED + "Multiple NPCs found by that name! Please use the -id flag instead.");
							return true;
						}
						plugin.getConfig().set("npcs." + plugin.npcManager.getID(npc) + ".radius", radius);
						plugin.saveConfig();
						sender.sendMessage(ChatColor.GREEN + "Radius saved. NPC with name '" + name + "' now has aware radius of " + ChatColor.GRAY + radius);
						if(plugin.debug) plugin.log(ChatColor.GREEN + "Aware radius for NPC '" + name + "' is now set to: " + radius);
						return true;
					}
				}
				plugin.getConfig().set("look-at-radius", radius);
				plugin.saveConfig();
				plugin.lookAtRadius = radius;
				sender.sendMessage(ChatColor.GREEN + "Radius saved. All NPCs aware radius is now " + ChatColor.GRAY + radius);
				if(plugin.debug) plugin.log(ChatColor.GREEN + "Aware radius is now set to: " + radius);
				return true;
			} else if (args[0].equalsIgnoreCase("reload")) {
				if (!isAuthorized(sender, "dwdnpc.admin")) return dispNoPerms(sender);
				if(plugin.debug) plugin.log(ChatColor.GREEN + "Reloading config.yml");
				plugin.reloadConfig();
				plugin.loadConfig();
				if(plugin.debug) plugin.log(ChatColor.GREEN + "Removing all loaded NPCs.");
				plugin.npcManager.despawnAll();
				if(plugin.debug) plugin.log(ChatColor.GREEN + "Loading back NPCs from config.");
				plugin.npcManager.loadNPCs();
				if(plugin.debug) plugin.log(ChatColor.GREEN + "Restarting LookAtTask");
				plugin.stopDelayedTask();
				plugin.startDelayedTask();
				sender.sendMessage(ChatColor.GREEN + "All NPCs have been reloaded from config!");
				return true;
			} else return showusage(sender);
		}
		return false;
	}
}
