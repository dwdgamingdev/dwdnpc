package net.downwithdestruction.dwdnpc.npclib.entity;

import net.downwithdestruction.dwdnpc.DwDNPC;
import net.downwithdestruction.dwdnpc.npclib.mob.Mob;
import net.downwithdestruction.dwdnpc.npclib.nms.NPCEntity;
import net.minecraft.server.v1_5_R2.EntityPlayer;
import net.minecraft.server.v1_5_R2.Packet;
import net.minecraft.server.v1_5_R2.Packet5EntityEquipment;
import net.minecraft.server.v1_5_R2.WorldServer;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_5_R2.CraftServer;
import org.bukkit.craftbukkit.v1_5_R2.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_5_R2.inventory.CraftItemStack;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class HumanNPC extends NPC {
	private Double lookAtRadius;
	private Double messageRadius;
	private String message;
	
	public HumanNPC(NPCEntity npcEntity) {
		super(npcEntity);
	}
	
	public void setName(String name) {
		((NPCEntity) getEntity()).name = name;
	}
	
	public String getName() {
		return ((NPCEntity) getEntity()).name;
	}
	
	public void lookAtPoint(Location point) {
		if (getEntity().getBukkitEntity().getWorld() != point.getWorld()) {
			return;
		}
		Location npcLoc = ((LivingEntity) getEntity().getBukkitEntity()).getEyeLocation();
		double xDiff = point.getX() - npcLoc.getX();
		double yDiff = point.getY() - npcLoc.getY();
		double zDiff = point.getZ() - npcLoc.getZ();
		double DistanceXZ = Math.sqrt(xDiff * xDiff + zDiff * zDiff);
		double DistanceY = Math.sqrt(DistanceXZ * DistanceXZ + yDiff * yDiff);
		double newYaw = Math.acos(xDiff / DistanceXZ) * 180 / Math.PI;
		double newPitch = Math.acos(yDiff / DistanceY) * 180 / Math.PI - 90;
		if (zDiff < 0.0) {
			newYaw = newYaw + Math.abs(180 - newYaw) * 2;
		}
		getEntity().yaw = (float) (newYaw - 90);
		getEntity().pitch = (float) newPitch;
		((EntityPlayer)getEntity()).az = (float)(newYaw - 90);
		final Integer id = DwDNPC.getPlugin().npcManager.getNPCIdFromEntity(getBukkitEntity());
		if (id == null) return;
		if (DwDNPC.getPlugin().npcManager.isMob(id)) {
			Mob mob = DwDNPC.getPlugin().npcManager.mobDB.get(getName());
			Packet packet1 = mob.getEntityLookPacket(getPlayer().getEyeLocation());
			Packet packet2 = mob.getHeadRotatePacket(getPlayer().getEyeLocation());
			DwDNPC.getPlugin().npcManager.sendPacketToWorld(getPlayer().getWorld(), packet1, packet2);
		}
	}
	
	public Player getPlayer() {
		if (!(getEntity().getBukkitEntity() instanceof CraftPlayer)) {
			((NPCEntity) getEntity()).setBukkitEntity(new CraftPlayer((CraftServer) Bukkit.getServer(), (EntityPlayer) getEntity()));
		}
		return (Player) getEntity().getBukkitEntity();
	}
	
	public void updateArmor(int slot, ItemStack itm) {
		/* SLOT NUMBERS
			0 = Item Held in Hand
			1 = Boots
			2 = Leggings
			3 = Chestplate
			4 = Helmet */
		net.minecraft.server.v1_5_R2.ItemStack i = CraftItemStack.asNMSCopy(itm);
		Packet p = new Packet5EntityEquipment(getEntity().id, slot, i);
		((WorldServer) getEntity().world).tracker.a(getEntity(), p);
	}
	
	public void setBoots(int id) {
		ItemStack item = new ItemStack(id);
		getPlayer().getInventory().setBoots(item);
		updateArmor(1, item);
	}
	
	public void setLeggings(int id) {
		ItemStack item = new ItemStack(id);
		getPlayer().getInventory().setLeggings(item);
		updateArmor(2, item);
	}
	
	public void setChestplate(int id) {
		ItemStack item = new ItemStack(id);
		getPlayer().getInventory().setChestplate(item);
		updateArmor(3, item);
	}
	
	public void setHelmet(int id) {
		ItemStack item = new ItemStack(id);
		getPlayer().getInventory().setHelmet(item);
		updateArmor(4, item);
	}
	
	public void setHand(int id) {
		ItemStack item = new ItemStack(id);
		getPlayer().getInventory().setItemInHand(item);
		updateArmor(0, item);
	}
	
	public Integer getBoots() {
		final ItemStack boots = getPlayer().getInventory().getBoots();
		if (boots == null) return 0;
		return boots.getTypeId();
	}
	
	public Integer getLeggings() {
		final ItemStack leggings = getPlayer().getInventory().getLeggings();
		if (leggings == null) return 0;
		return leggings.getTypeId();
	}
	
	public Integer getChestplate() {
		final ItemStack chestplate = getPlayer().getInventory().getChestplate();
		if (chestplate == null) return 0;
		return chestplate.getTypeId();
	}
	
	public Integer getHelmet() {
		final ItemStack helmet = getPlayer().getInventory().getHelmet();
		if (helmet == null) return 0;
		return helmet.getTypeId();
	}
	
	public Integer getHand() {
		final ItemStack hand = getPlayer().getInventory().getItemInHand();
		if (hand == null) return 0;
		return hand.getTypeId();
	}
	
	public void setLookAtRadius(Double radius) {
		lookAtRadius = radius;
	}
	
	public void setMessageRadius(Double radius) {
		messageRadius = radius;
	}
	
	public void setMessage(String msg) {
		message = msg;
	}
	
	public Double getLookAtRadius(Double radius) {
		return (lookAtRadius != null) ? lookAtRadius : radius;
	}
	
	public Double getMessageRadius() {
		return messageRadius;
	}
	
	public String getMessage() {
		return message;
	}
}
