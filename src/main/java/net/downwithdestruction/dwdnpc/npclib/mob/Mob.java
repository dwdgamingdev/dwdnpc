package net.downwithdestruction.dwdnpc.npclib.mob;

import java.util.LinkedList;
import java.util.Map;
import net.downwithdestruction.dwdnpc.DwDNPC;
import net.downwithdestruction.dwdnpc.packet.PacketGenerator;
import net.minecraft.server.v1_5_R2.DataWatcher;
import net.minecraft.server.v1_5_R2.Packet24MobSpawn;
import net.minecraft.server.v1_5_R2.Packet29DestroyEntity;
import net.minecraft.server.v1_5_R2.Packet32EntityLook;
import net.minecraft.server.v1_5_R2.Packet35EntityHeadRotation;
import net.minecraft.server.v1_5_R2.Packet40EntityMetadata;
import org.bukkit.ChatColor;
import org.bukkit.Location;
//import net.minecraft.server.v1_4_6.Packet5EntityEquipment;

public class Mob {
	public int entityID;
	public LinkedList<String> data = new LinkedList<String>();
	public MobType type;
	public DataWatcher metadata;
	public PacketGenerator packetGenerator;
	
	public Mob(int entityID, LinkedList<String> data, MobType type) {
		this.entityID = entityID;
		this.data = data;
		this.type = type;
		sharedConstruct();
	}
	
	public Mob(int entityID, String data, MobType type) {
		this.entityID = entityID;
		this.data.addFirst(data);
		this.type = type;
		sharedConstruct();
	}
	
	public Mob(int entityID, MobType type) {
		this.entityID = entityID;
		this.type = type;
		sharedConstruct();
	}
	
	protected void sharedConstruct() {
		packetGenerator = new PacketGenerator(this);
		dataCheck();
		initializeData();
		handleData();
	}
	
	protected void dataCheck() {
		if (data.isEmpty()) {
			data.add("Glaciem"); // TODO what is this?
		}
	}
	
	public Mob setEntityID(int entityID) {
		this.entityID = entityID;
		return this;
	}
	
	public Mob setData(LinkedList<String> data) {
		this.data = data;
		initializeData();
		handleData();
		return this;
	}
	
	public Mob setSingleData(String data) {
		this.data.clear();
		this.data.addFirst(data);
		metadata = new DataWatcher();
		initializeData();
		handleData();
		return this;
	}
	
	public Mob addSingleData(String data) {
		if (!this.data.contains(data)) {
			this.data.add(data);
		}
		initializeData();
		handleData();
		return this;
	}
	
	public Mob clearData() {
		data.clear();
		dataCheck();
		return this;
	}
	
	public Mob setType(MobType type) {
		this. type =  type;
		initializeData();
		return this;
	}
	
	public void initializeData() {
		metadata = type.newMetadata();
		safeAddData(0, (Object) (byte) 0, false);
		safeAddData(12, (Object) 0, false);
		if (type == MobType.Bat) {
			metadata.watch(16, (byte) -2);
		}
	}
	
	@SuppressWarnings("rawtypes")
	public void safeAddData(int index, Object value, boolean forcemodify) {
		try {
			if (((Map) MobType.mapField.get(metadata)).containsKey(index)) {
				if (forcemodify) {
					metadata.watch(index, value);
				}
			} else {
				metadata.a(index, value);
			}
		} catch (IllegalArgumentException e) {
			DwDNPC.getPlugin().log(ChatColor.RED + "[SEVERE] Something bad happened in a " + type.name() + " disguise!");
		} catch (IllegalAccessException e) {
			DwDNPC.getPlugin().log(ChatColor.RED + "[SEVERE] Could not access the metadata map for a " + type.name() + " disguise!");
		}
	}
	
	public void handleData() {
		if (!data.isEmpty()) {
			byte firstIndex = 0;
			if (data.contains("burning")) firstIndex = (byte) (firstIndex | 0x01);
			if (data.contains("crouched")) firstIndex = (byte) (firstIndex | 0x02);
			if (data.contains("riding")) firstIndex = (byte) (firstIndex | 0x04);
			if (data.contains("sprinting")) firstIndex = (byte) (firstIndex | 0x08);
			metadata.watch(0, firstIndex);
			if (data.contains("baby")) metadata.watch(12, -23999);
			else metadata.watch(12, 0);
			if (data.contains("black")) metadata.watch(16, (byte) 15);
			else if (data.contains("blue")) metadata.watch(16, (byte) 11);
			else if (data.contains("brown")) metadata.watch(16, (byte) 12);
			else if (data.contains("cyan")) metadata.watch(16, (byte) 9);
			else if (data.contains("gray")) metadata.watch(16, (byte) 7);
			else if (data.contains("green")) metadata.watch(16, (byte) 13);
			else if (data.contains("lightblue")) metadata.watch(16, (byte) 3);
			else if (data.contains("lime")) metadata.watch(16, (byte) 5);
			else if (data.contains("magenta")) metadata.watch(16, (byte) 2);
			else if (data.contains("orange")) metadata.watch(16, (byte) 1);
			else if (data.contains("pink")) metadata.watch(16, (byte) 6);
			else if (data.contains("purple")) metadata.watch(16, (byte) 10);
			else if (data.contains("red")) metadata.watch(16, (byte) 14);
			else if (data.contains("silver")) metadata.watch(16, (byte) 8);
			else if (data.contains("white")) metadata.watch(16, (byte) 0);
			else if (data.contains("yellow")) metadata.watch(16, (byte) 4);
			else if (data.contains("sheared")) metadata.watch(16, (byte) 16);
			if (data.contains("charged")) metadata.watch(17, (byte) 1);
			try {
				if (data.contains("tiny")) metadata.watch(16, (byte) 1);
				else if (data.contains("small")) metadata.watch(16, (byte) 2);
				else if (data.contains("big")) metadata.watch(16, (byte) 4);
			} catch (Exception e) {
				DwDNPC.getPlugin().log(ChatColor.GOLD + "[WARNING] Bad cube size values in configuration!");
			}
			if (data.contains("sitting")) {
				try { metadata.a(16, (byte) 1); }
				catch (IllegalArgumentException e) { metadata.watch(16, (byte) 1); }
			} else if (data.contains("aggressive")) {
				if (type == MobType.Wolf) {
					try { metadata.a(16, (byte) 2); }
					catch (IllegalArgumentException e) { metadata.watch(16, (byte) 2); }
				} else if (type == MobType.Ghast) metadata.watch(16, (byte) 1);
				else if (type == MobType.Enderman) metadata.watch(17, (byte) 1);
			} else if (data.contains("tamed")) {
				try { metadata.a(16, (byte) 4); }
				catch (IllegalArgumentException e) { metadata.watch(16, (byte) 4); }
			}
			if (data.contains("tabby")) metadata.watch(18, (byte) 2);
			else if (data.contains("tuxedo")) metadata.watch(18, (byte) 1);
			else if (data.contains("siamese")) metadata.watch(18, (byte) 3);
			if (data.contains("saddled")) metadata.watch(16, (byte) 1);
			Byte held = getBlockID();
			if (held != null && type == MobType.Enderman) {
				metadata.watch(16, held.byteValue());
				Byte blockData = getBlockData();
				if (blockData != null) safeAddData(17, blockData.byteValue(), true);
			}
			if (data.contains("farmer")) metadata.watch(16, 0);
			else if (data.contains("librarian")) metadata.watch(16, 1);
			else if (data.contains("priest")) metadata.watch(16, 2);
			else if (data.contains("blacksmith")) metadata.watch(16, 3);
			else if (data.contains("butcher")) metadata.watch(16, 4);
		}
	}
	
	public Byte getBlockID() {
		if (!data.isEmpty()) {
			for (String one : data) {
				if (one.startsWith("blockID:")) {
					String[] parts = one.split(":");
					try {
						return Byte.valueOf(parts[1]);
					} catch (NumberFormatException e) {
						DwDNPC.getPlugin().log(ChatColor.GOLD + "[WARNING] Could not parse the byte of a disguise's block!");
					}
				}
			}
		}
		return null;
	}
	
	public Byte getBlockData() {
		if (!data.isEmpty()) {
			for (String one : data) {
				if (one.startsWith("blockData:")) {
					String[] parts = one.split(":");
					try {
						return Byte.decode(parts[1]);
					} catch (NumberFormatException e) {
						DwDNPC.getPlugin().log(ChatColor.GOLD + "[WARNING] Could not decode the byte of a disguise's block data!");
					}
				}
			}
		}
		return null;
	}
	
	public Packet24MobSpawn getMobSpawnPacket(Location loc) {
		return packetGenerator.getMobSpawnPacket(loc);
	}
	
	public Packet29DestroyEntity getEntityDestroyPacket() {
		return packetGenerator.getEntityDestroyPacket();
	}
	
	/*public Packet5EntityEquipment getEquipmentChangePacket(short slot, ItemStack item) {
		return packetGenerator.getEquipmentChangePacket(slot, item);
	}*/
	
	public Packet32EntityLook getEntityLookPacket(Location loc) {
		return packetGenerator.getEntityLookPacket(loc);
	}
	
	public Packet40EntityMetadata getEntityMetadataPacket() {
		return packetGenerator.getEntityMetadataPacket();
	}
	
	public Packet35EntityHeadRotation getHeadRotatePacket(Location loc) {
		return packetGenerator.getHeadRotatePacket(loc);
	}
}
