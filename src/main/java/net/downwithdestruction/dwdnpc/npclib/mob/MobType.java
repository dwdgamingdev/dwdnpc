package net.downwithdestruction.dwdnpc.npclib.mob;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.LinkedList;
import net.downwithdestruction.dwdnpc.DwDNPC;
import net.minecraft.server.v1_5_R2.DataWatcher;
import net.minecraft.server.v1_5_R2.WatchableObject;
import org.bukkit.ChatColor;

public enum MobType {
	Bat(65),
	Blaze(61),
	CaveSpider(59),
	Chicken(93),
	Cow(92),
	Creeper(50),
	EnderDragon(63),
	Enderman(58),
	Ghast(56),
	Giant(53),
	IronGolem(99),
	MagmaCube(62),
	MushroomCow(96),
	Ocelot(98),
	Pig(90),
	PigZombie(57),
	Sheep(91),
	Silverfish(60),
	Skeleton(51),
	Slime(55),
	Snowman(97),
	Spider(52),
	Squid(94),
	Villager(120),
	Witch(66),
	Wither(64),
	Wolf(95),
	Zombie(54);
	
	public static LinkedList<MobType> missingDisguises = new LinkedList<MobType>();
	protected static HashMap<Byte, DataWatcher> modelData = new HashMap<Byte, DataWatcher>();
	public static Field mapField;
	public final byte id;
	
	static {
		try {
			Field watcherField = net.minecraft.server.v1_5_R2.Entity.class.getDeclaredField("datawatcher");
			watcherField.setAccessible(true);
			/*for (MobType m : values()) {
				String mobClass = "net.minecraft.server.Entity" + m.name();
				if (m == MobType.Giant) mobClass = mobClass + "Zombie";
				try {
					Entity ent = (Entity) Class.forName(mobClass).getConstructor(net.minecraft.server.v1_4_6.World.class).newInstance((Object) null);
					modelData.put(m.id, (DataWatcher) watcherField.get(ent));
				} catch (Exception e) { missingDisguises.add(m); }
			}*/
		} catch (Exception e) { DwDNPC.getPlugin().log(ChatColor.RED + "[SEVERE] Could not access datawatchers!"); }
		try {
			mapField = DataWatcher.class.getDeclaredField("b");
			mapField.setAccessible(true);
		} catch (NoSuchFieldException e) { DwDNPC.getPlugin().log(ChatColor.RED + "[SEVERE] Could not find datawatcher map field: b"); }
		catch (SecurityException e) { DwDNPC.getPlugin().log(ChatColor.RED + "[SEVERE] Could not access datawatcher map field: b"); }
	}
	
	MobType(int i) {
		id = (byte) i;
	}
	
	public boolean isSubclass(Class<?> cls) {
		try { return cls.isAssignableFrom(Class.forName("org.bukkit.entity." + name())); }
		catch (ClassNotFoundException e) { e.printStackTrace(); }
		return false;
	}
	
	public static MobType fromString(String text) {
		for (MobType m : MobType.values()) {
			if (text.equalsIgnoreCase(m.name())) {
				if (missingDisguises.contains(m)) return null;
				else return m;
			}
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public DataWatcher newMetadata() {
		if (modelData.containsKey(id)) {
			DataWatcher model = modelData.get(id);
			DataWatcher w = new DataWatcher();
			int i = 0;
			for (Field f : model.getClass().getDeclaredFields()) {
				f.setAccessible(true);
				if (i == 1) {
					try {
						HashMap<Integer, WatchableObject> modelMap = ((HashMap<Integer, WatchableObject>) f.get(model));
						HashMap<Integer, WatchableObject> newMap = ((HashMap<Integer, WatchableObject>) f.get(w));
						for (Integer index : modelMap.keySet()) newMap.put(index, copyWatchable(modelMap.get(index)));
					} catch (Exception e) {
						DwDNPC.getPlugin().log(ChatColor.RED + "[SEVERE] Could not clone hashmap" + i + " in a " + this.name() + "'s model datawatcher!");
					}
				} else if (i == 2) {
					try { f.setBoolean(w, f.getBoolean(model)); }
					catch (Exception e) {
						DwDNPC.getPlugin().log(ChatColor.RED + "[SEVERE] Could not clone boolean" + i + " in a " + this.name() + "'s model datawatcher!");
					}
				}
				i++;
			}
			return w;
		} else return new DataWatcher();
	}
	
	private WatchableObject copyWatchable(WatchableObject watchable) {
		return new WatchableObject(watchable.c(), watchable.a(), watchable.b());
	}
	
	public static String subTypes = "baby, black, blue, brown, cyan, " +
		"gray, green, lightblue, lime, magenta, orange, pink, purple, red, " +
		"silver, white, yellow, sheared, charged, tiny, small, big, bigger, massive, godzilla, " +
		"tamed, aggressive, tabby, tuxedo, siamese, burning, saddled, " +
		"librarian, priest, blacksmith, butcher";
}