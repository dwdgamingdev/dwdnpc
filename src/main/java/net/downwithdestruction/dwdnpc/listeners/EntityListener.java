package net.downwithdestruction.dwdnpc.listeners;

import net.downwithdestruction.dwdnpc.DwDNPC;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityTargetEvent;
import org.bukkit.event.entity.EntityTargetLivingEntityEvent;

public class EntityListener implements Listener {
	private DwDNPC plugin;
	
	public EntityListener(DwDNPC plugin) {
		this.plugin = plugin;
	}
	
	@EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onDamage(EntityDamageEvent event) {
		if (plugin.npcManager.getNPCFromEntity(event.getEntity()) !=  null) {
			event.setCancelled(true);
			event.setDamage(-event.getDamage());
		}
	}
	
	/* ***************************************************************************************************
	 ** NOTE: SLIMES/MAGMACUBES WILL STILL ATTACK NPCs: https://bukkit.atlassian.net/browse/BUKKIT-1408 **
	 *************************************************************************************************** */
	
	@EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onEntityTarget(EntityTargetEvent event) {
		if (plugin.npcManager.getNPCFromEntity(event.getTarget()) !=  null) {
			event.setCancelled(true);
		}
	}
	
	@EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onEntityTargetLivingEntity(EntityTargetLivingEntityEvent event) {
		if (plugin.npcManager.getNPCFromEntity(event.getTarget()) !=  null) {
			event.setCancelled(true);
		}
	}
}
