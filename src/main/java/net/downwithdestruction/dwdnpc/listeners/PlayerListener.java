package net.downwithdestruction.dwdnpc.listeners;

import net.downwithdestruction.dwdnpc.DwDNPC;
import net.downwithdestruction.dwdnpc.npclib.entity.HumanNPC;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;

public class PlayerListener implements Listener {
	private DwDNPC plugin;
	
	public PlayerListener(DwDNPC plugin) {
		this.plugin = plugin;
	}
	
	@EventHandler(ignoreCancelled = true)
	public void onPlayerInteractEntity(PlayerInteractEntityEvent event) {
		if (event.isCancelled())return;
		final Player player = event.getPlayer();
		final Integer npcID = plugin.npcManager.getNPCIdFromEntity(event.getRightClicked());
		if (npcID == null)
			return; // Not Right Clicked
		final HumanNPC npc = (HumanNPC) plugin.npcManager.getNPCByID(npcID);
		if (npc == null)
			return; // Not an NPC
		final int id = player.getItemInHand().getTypeId();
		switch (id) {
			// BOOTS ARMOR
			case 317: // Gold
			case 313: // Diamond
			case 309: // Iron
			case 305: // Chainmail
			case 301: // Leather
				if (!player.hasPermission("dwdnpc.setarmor"))
					return; // No Permission For Action
				
				if (npc.getBoots() == id) {
					npc.setBoots(0);
					plugin.getConfig().set("npcs." + npcID + ".armor.boots", null);
					armorCheck(npc, npcID);
				} else {
					npc.setBoots(id);
					plugin.getConfig().set("npcs." + npcID + ".armor.boots", id);
				}
				plugin.saveConfig();
				break;
			// LEGGINGS ARMOR
			case 316: // Gold
			case 312: // Diamond
			case 308: // Iron
			case 304: // Chainmail
			case 300: // Leather
				if (!player.hasPermission("dwdnpc.setarmor"))
					return; // No Permission For Action
				if (npc.getLeggings() == id) {
					npc.setLeggings(0);
					plugin.getConfig().set("npcs." + npcID + ".armor.leggings", null);
					armorCheck(npc, npcID);
				} else {
					npc.setLeggings(id);
					plugin.getConfig().set("npcs." + npcID + ".armor.leggings", id);
				}
				plugin.saveConfig();
				break;
			// CHESTPLATE ARMOR
			case 315: // Gold
			case 311: // Diamond
			case 307: // Iron
			case 303: // Chainmail
			case 299: // Leather
				if (!player.hasPermission("dwdnpc.setarmor"))
					return; // No Permission For Action
				if (npc.getChestplate() == id) {
					npc.setChestplate(0);
					plugin.getConfig().set("npcs." + npcID + ".armor.chestplate", null);
					armorCheck(npc, npcID);
				} else {
					npc.setChestplate(id);
					plugin.getConfig().set("npcs." + npcID + ".armor.chestplate", id);
				}
				plugin.saveConfig();
				break;
			// HELMET ARMOR
			case 314: // Gold
			case 310: // Diamond
			case 306: // Iron
			case 302: // Chainmail
			case 298: // Leather
				if (!player.hasPermission("dwdnpc.setarmor"))
					return; // No Permission For Action
				if (npc.getHelmet() == id) {
					npc.setHelmet(0);
					plugin.getConfig().set("npcs." + npcID + ".armor.helmet", null);
					armorCheck(npc, npcID);
				} else {
					npc.setHelmet(id);
					plugin.getConfig().set("npcs." + npcID + ".armor.helmet", id);
				}
				plugin.saveConfig();
				break;
			// AIR (Show ID of NPC)
			case 0:
				if (player.hasPermission("dwdnpc.delete"))
					player.sendMessage(ChatColor.LIGHT_PURPLE + "This NPC's ID is " + ChatColor.GRAY + npcID.toString() + ChatColor.LIGHT_PURPLE + ".");
				break;
			// ALL OTHERS (Place item in hand)
			default:
				if (!player.hasPermission("dwdnpc.setarmor"))
					return; // No Permission For Action
				if (npc.getHand() == id) {
					npc.setHand(0);
					plugin.getConfig().set("npcs." + npcID + ".in-hand", null);
				} else {
					npc.setHand(id);
					plugin.getConfig().set("npcs." + npcID + ".in-hand", id);
				}
				plugin.saveConfig();
				break;
		}
	}
	
	void armorCheck(HumanNPC npc, int npcID) {
		if ((npc.getPlayer().getInventory().getBoots() == null) &&
				(npc.getPlayer().getInventory().getLeggings() == null) &&
				(npc.getPlayer().getInventory().getChestplate() == null) &&
				(npc.getPlayer().getInventory().getHelmet() == null)) {
			plugin.getConfig().set("npcs." + npcID + ".armor", null);
		}
	}
}
