package net.downwithdestruction.dwdnpc;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;

import net.downwithdestruction.dwdnpc.Metrics.Graph;
import net.downwithdestruction.dwdnpc.listeners.*;
import net.downwithdestruction.dwdnpc.npclib.NPCManager;
import net.downwithdestruction.dwdnpc.runnables.*;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.kitteh.vanish.VanishPlugin;

public class DwDNPC extends JavaPlugin {
	private final PluginManager pluginManager = Bukkit.getPluginManager();
	private static DwDNPC instance;
	private VanishPlugin vp = null;
	private Boolean colorLogs = true;
	private Integer taskDelay;
	private Integer taskId;
	private Integer skinMobsDelay;
	
	public NPCManager npcManager;
	public Double lookAtRadius;
	public Double skinMobsRadius;
	public Boolean debug = false;
	public ConfigurationSection npcs;
	
	protected int nextID = Integer.MIN_VALUE;
	public int getNextAvailableID() {
		return nextID++;
	}
	
	public void onEnable() {
		instance = this;
		loadConfig();
		if ((lookAtRadius < 1) || (lookAtRadius > 50)) lookAtRadius = 10D;
		vp = (VanishPlugin) getServer().getPluginManager().getPlugin("VanishNoPacket");
		npcManager = new NPCManager(this);
		npcManager.loadNPCs();
		pluginManager.registerEvents(new PlayerListener(this), this);
		pluginManager.registerEvents(new EntityListener(this), this);
		
		startDelayedTask();
		getServer().getScheduler().scheduleSyncRepeatingTask(this, new SkinMobsPacket(this), 0, skinMobsDelay);
		
		this.getCommand("dwdnpc").setExecutor(new CmdNPC(this));
		try {
			final Metrics metrics = new Metrics(this);
			final Graph graph = metrics.createGraph("Total Active NPCs");
			graph.addPlotter(new Metrics.Plotter("Total NPCs") { @Override public int getValue() { return npcManager.getNPCs().size(); } });
			metrics.start();
		} catch (IOException e) { log(ChatColor.RED + "Failed to start Metrics: " + ChatColor.YELLOW + e.getMessage()); }
		log(ChatColor.YELLOW + "v" + this.getDescription().getVersion() + " by BillyGalbreath enabled!");
	}
	
	public void onDisable() {
		log(ChatColor.YELLOW + "Plugin Disabled.");
	}
	
	public void loadConfig() {
		final File file = new File(getDataFolder() + File.separator + "config.yml");
		try {
			if (!file.exists()) {
				file.getParentFile().mkdirs();
				file.createNewFile();
				final FileConfiguration config = YamlConfiguration.loadConfiguration(file);
				config.addDefault("debug-mode", "false");
				config.addDefault("color-logs", "true");
				config.addDefault("look-at-radius", 10D);
				config.addDefault("look-at-delay", 3);
				config.addDefault("skin-mobs-radius", 50D);
				config.addDefault("skin-mobs-delay", 100);
				config.options().copyDefaults(true);
				config.save(file);
				if(debug) log(ChatColor.GOLD + "Created new default configuration file: " + file.getAbsolutePath());
			}
		} catch (IOException e) {
			if(debug) log(ChatColor.RED + "Config file at " + file.getAbsolutePath() + " failed to load: " + e.getMessage());
		}
		debug = getConfig().getBoolean("debug-mode", false);
		colorLogs = getConfig().getBoolean("color-logs", true);
		lookAtRadius = getConfig().getDouble("look-at-radius", 10D);
		taskDelay = getConfig().getInt("look-at-delay", 3);
		skinMobsRadius = getConfig().getDouble("skin-mobs-radius", 50D);
		skinMobsDelay = getConfig().getInt("skin-mobs-delay", 100);
		npcs = getConfig().getConfigurationSection("npcs");
	}
	
	public void startDelayedTask() {
		taskId = getServer().getScheduler().scheduleSyncRepeatingTask(this, new DelayedTask(this), 0, taskDelay);
	}
	
	public void stopDelayedTask() {
		Bukkit.getScheduler().cancelTask(taskId);
	}
	
	public boolean withinRange(Location loc, Location pLoc, double range) {
		if (loc == null || pLoc == null || loc.getWorld() != pLoc.getWorld()) return false;
		return Math.pow(range, 2) > loc.distanceSquared(pLoc);
	}
	
	public boolean isVanished(Player p) {
		if (vp == null) {
			vp = (VanishPlugin) Bukkit.getServer().getPluginManager().getPlugin("VanishNoPacket");
			return false;
		} else return vp.getManager().isVanished(p.getName());
	}
	
	public void log (final Object obj) {
		if (colorLogs) {
			getServer().getConsoleSender().sendMessage(
					ChatColor.AQUA + "[" + ChatColor.LIGHT_PURPLE + getName() + ChatColor.AQUA + "] " + ChatColor.RESET + obj
			);
		} else {
			Bukkit.getLogger().log(Level.INFO, "[" + getName() + "] " + ((String) obj).replaceAll("(?)\u00a7([a-f0-9k-or])", ""));
		}
	}
	
	public static DwDNPC getPlugin() {
		return instance;
	}
}
